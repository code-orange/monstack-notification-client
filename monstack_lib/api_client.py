from urllib.parse import urljoin

import requests
from decouple import config


def handle(notification_mode, args):
    # Monitoring Stack
    MONSTACK_NOTIFY_API_URL = config(
        "MONSTACK_NOTIFY_API_URL", default="https://api.example.com", cast=str
    )
    MONSTACK_NOTIFY_API_USER = config(
        "MONSTACK_NOTIFY_API_USER", default="monstack", cast=str
    )
    MONSTACK_NOTIFY_API_PASSWD = config(
        "MONSTACK_NOTIFY_API_PASSWD", default="monstack", cast=str
    )

    if args.verbose:
        print("VERBOSITY turned on")

    request_url = urljoin(MONSTACK_NOTIFY_API_URL, "notifier/api/v1/notification")
    payload = dict()

    session = requests.Session()
    session.headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "ApiKey "
        + MONSTACK_NOTIFY_API_USER
        + ":"
        + MONSTACK_NOTIFY_API_PASSWD,
    }

    payload["check_type"] = notification_mode
    payload["long_date_time"] = args.d
    payload["hostname"] = args.l
    payload["state"] = args.s
    payload["notification_type"] = args.t
    payload["comment"] = args.c

    if notification_mode == "service":
        payload["service_name"] = args.e

    # create arguments for the request
    request_args = {"url": request_url, "json": payload, "verify": False}

    # do the request
    response = session.post(**request_args)

    if args.verbose:
        print(response.content)

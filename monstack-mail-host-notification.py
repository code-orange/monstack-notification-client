#!/bin/python3

import argparse

from monstack_lib.api_client import handle

NOTIFICATION_MODE = "host"

parser = argparse.ArgumentParser()
parser.add_argument("--verbose", help="increase output verbosity", action="store_true")
parser.add_argument("-d", help="LONGDATETIME", type=str)
parser.add_argument("-l", help="HOSTNAME", type=str)
parser.add_argument("-o", help="HOSTOUTPUT", type=str)
parser.add_argument("-s", help="HOSTSTATE", type=str)
parser.add_argument("-t", help="NOTIFICATIONTYPE", type=str)
parser.add_argument("-c", help="NOTIFICATIONCOMMENT", type=str)

args = parser.parse_args()

handle(NOTIFICATION_MODE, args)
